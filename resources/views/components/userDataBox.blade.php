@component('components.box')
    <div class="row">
        <div class="col-md-5 text-center">
            @if(!isset($profilePicture) or !$profilePicture)
                <img class="profile-img" src="{{\Illuminate\Support\Facades\Storage::disk('local')->url('public/profile/pictures/ninja.png')}}">
            @else
                <img class="profile-img" src="{{\Illuminate\Support\Facades\Storage::disk('local')->url($profilePicture)}}">
            @endif
        </div>
        <div class="col-md-6 text-center mt-4">
            <p>{{\App\Helpers\UserHelper::getCharacterName($user)}} ({{$user->sex}})</p>
            <p>{{$user->dateofbirth}}</p>
            <p>{{$user->height}} cm</p>
        </div>
    </div>
@endcomponent
