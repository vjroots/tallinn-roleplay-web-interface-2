<div class="card-header">
    <b>{{__('headers.user_data')}}</b>
</div>
{{ $slot }}
<div class="card-body">
    <p><strong>{{__('texts.steam_id')}}</strong> {{ $userData->identifier }}</p>
    <p><strong>{{__('texts.five_m_steam_user')}}: </strong>{{ $userData->name }}</p>
    <p><strong>{{__('texts.character_name')}}: </strong>{{ $userData->firstname }} {{$userData->lastname}}</p>
    <p><strong>{{__('texts.date_of_birth')}}: </strong>{{ $userData->dateofbirth }}</p>
    <p><strong>{{__('texts.sex')}}: </strong>{{ $userData->sex }}</p>
    <p><strong>{{__('texts.height')}}: </strong>{{ $userData->height }}</p>
    @if(\Illuminate\Support\Facades\Auth::user()->fiveMUser()->first()->identifier === $userData->identifier)
        <p><strong>{{__('texts.job')}}: </strong>
            {{ \App\Job::where('name', $userData->job)->first()->label }}
            {{ \App\JobGrades::where('job_name', $userData->job)->where('grade', $userData->job_grade)->first()->label }}
        </p>
        <p><strong>{{__('texts.cash')}}: </strong>{{ $userData->money }}</p>
        <p><strong>{{__('texts.money_on_account')}}: </strong>{{ $userData->bank }}</p>
    @endif
</div>
