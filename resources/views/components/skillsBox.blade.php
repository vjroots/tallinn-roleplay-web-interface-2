@component('components.box')
    @if ($skills)
        @foreach($skills as $skill)
            <div class="row mt-4">
                <div class="col-md-3"><strong>{{__('texts.' . $skill->type)}}</strong></div>
                <div class="col-md-9">
                    <div class="progress align-self-center" style="background-color: #b9bbbe;" title="{{__('texts.' . $skill->type)}}">
                        <div class="progress-bar" role="progressbar" style="width: {{\App\Helpers\SkillsHelper::getPercentOf($skill->amount)}}%"
                        aria-valuenow="{{$skill->amount}}" aria-valuemin="0" aria-valuemax="100"
                        title="{{__('texts.' . $skill->type)}}: {{$skill->amount}}/100"></div>
                  </div>
              </div>
            </div>
        @endforeach
    @endif
@endcomponent
