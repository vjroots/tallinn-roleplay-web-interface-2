<div class="pb-5">
    <form class="form-inline" action="{{route($routeName)}}" method="get">
        <input type="text" class="form-control col-md-9 typeahead" id="search" name="search" placeholder="{{$placeholder}}"
               @isset($oldSearch) value="{{$oldSearch}}" @endisset autocomplete="off">
        <button type="submit" class="btn btn-primary col-md-2 offset-md-1" onclick="overlayOn()"
                onkeypress="return keypress(event)">
            {{__('buttons.search')}}
        </button>
    </form>
</div>
@php
    $autocompleteUrl = !isset($autocompleteUrl) ? 'userSearchAutocomplete' : $autocompleteUrl
@endphp
<script>
    window.onload = function () {

        let path = "{{ route($autocompleteUrl) }}";

        $('input.typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1,
            autocomplete: false,
            source: function (query, process) {
                return $.get(path, {query: query}, function (data) {
                    return process(data);

                });

            }
        });
    }

    function keypress(event) {
        if (event.which == 13 || event.keyCode == 13) {
            overlayOn();
            return false;
        }
        return true;
    };
</script>
