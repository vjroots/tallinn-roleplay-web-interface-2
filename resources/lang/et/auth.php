<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Need kasutaja tunnused on valed.',
    'throttle' => 'Liiga palju sisse logimise katsed. Proovi uuesti :seconds sekundi pärast.',

];
