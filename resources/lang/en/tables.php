<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    | This file contains translations of buttons
    |
    */

    'label'=> 'Label',
    'issuer'=> 'Issuer',
    'receive'=> 'Receive',
    'amount'=> 'Amount',
    'actions'=> 'Actions',
    'character_name' => 'Character Name',
    'date_of_birth' => 'Date of birth',
    'is_wanted' => 'Is Wanted',
    'wanted_reason' => 'Wanted Reason',
    'declared_wanted_by' => 'Declared wanted by',
    'date' => 'Date',
    'picture' => 'Picture',
    'number_plate' => 'Number Plate',
    'model_name' => 'Model Name',
    'is_insured' => 'Is insured',
    'is_stolen' => 'Is stolen?',
    'insurance_end_date' => 'Insurance end date',
    'sell_car' => 'Sell car',
    'sell_request_nr' => 'Sell request Nr.',
    'jail_time' => 'Jail time',
    'summary' => 'Summary',
    'is_criminal_case' => 'Is criminal case',
    'created_at' => 'Created at',
    'criminal_case_expires_at' => 'Criminal case expires at',
    'owner_steam_name' => 'Owner Steam Name',
    'owner_name' => 'Owner Name',
    'officer_name' => 'Officer Name',
    'description' => 'Description',
    'category' => 'Category',
    'min_jail' => 'Min Jail',
    'max_jail' => 'Max Jail',
    'steam_name' => 'Steam Name',
    'job' => 'Job',
    'sex' => 'Sex',
    'request_nr' => 'Request Nr.',
    'seller' => 'Seller',
    'buyer' => 'Buyer',
    'approver' => 'Approver',
    'name' => 'Name',
    'model' => 'Model',
    'price' => 'Price',
    'last_rejection_reason' => 'Last Rejection Reason',
    'steam_id' => 'Steam id',
    'job_type' => 'Job type',
    'job_price' => 'Job price',
    'service_type' => 'Service type',
    'worker' => 'Worker',
    'com_service_min' => 'Min community service',
    'com_service_max' => 'Max community service',
    'total' => 'Total amount',
    'phone' => 'Phone Number',
    'job_grade' => 'Job Grade',
    'call_sign' => 'Call Sign',
];
