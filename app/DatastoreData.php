<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatastoreData extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'datastore_data';
}
