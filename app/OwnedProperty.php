<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnedProperty extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'owned_properties';
}
