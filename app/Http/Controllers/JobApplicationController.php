<?php
namespace App\Http\Controllers;

use App\CriminalRecord;
use App\Helpers\Sql\WebUserHelper;
use App\JobApplication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class JobApplicationController
 */
class JobApplicationController extends Controller
{

    /**
     * @var string
     */
    protected $steamId;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * View of police job application
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function policeApplicationIndex()
    {

        if(!$this->isUserReady()) {
            return redirect(route('whitelistApplicationStatus'));
        }

        $criminalCases = Auth::user()->getFiveMUserData()->criminalRecords()->where('is_criminal_case', true)->get();

        if ($criminalCases->count() > 0) {
            foreach ($criminalCases as $case) {
                if (new \DateTime($case->criminal_case_expires_at) > new \DateTime()) {
                    return redirect(route('home'))->with('error', 'You have criminal history. You can not apply for this job.');
                }
            }
        }

        if(Auth::user()->jobApplication()->where('isAccepted', false)->where('isRejected', false)->first()) {
            return redirect(route('home'))->with('error', 'You have already applied for a job.');
        }

        return view('jobApplications.police');
    }

    /**
     * View of ambulance job application
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function ambulanceApplicationIndex()
    {

        if(!$this->isUserReady()) {
            return redirect(route('whitelistApplicationStatus'));
        }
        
        $criminalCases = Auth::user()->getFiveMUserData()->criminalRecords()->where('is_criminal_case', true)->get();

        if ($criminalCases->count() > 0) {
            foreach ($criminalCases as $case) {
                if (new \DateTime($case->criminal_case_expires_at) > new \DateTime()) {
                    return redirect(route('home'))->with('error', 'You have criminal history. You can not apply for this job.');
                }
            }
        }

        if(Auth::user()->jobApplication()->where('isAccepted', false)->where('isRejected', false)->first()) {
            return redirect(route('home'))->with('error', 'You have already applied for a job.');
        }

        return view('jobApplications.ambulance');
    }

    /**
     * Store job application to database
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function storeApplication(Request $request)
    {

        $applicationData = $request->all();
        $job = $request->job;
        unset($applicationData['_token']);
        unset($applicationData['job']);

        $jobApplication = new JobApplication;
        $jobApplication->user_identifier = $this->getSteamId();
        $jobApplication->web_user_id = Auth::id();
        $jobApplication->answers = json_encode($applicationData);
        $jobApplication->job = $job;
        $jobApplication->save();

        return redirect(route('home'))->with('status', 'Job application sent');
    }

    /**
    * @return string steam id/hex
    */
    public function getSteamId()
    {

        if (!$this->steamId) {
            $this->steamId = Auth::user()->fiveMUser()->first()->identifier;
        }

        return $this->steamId;
    }

    /**
    * Check if user met all requirements so he/she can apply for job
    *
    * @return bool
    */
    private function isUserReady()
    {
        $whitelistApplication = Auth::user()->whitelistApplication()->first();

        if (
            !$whitelistApplication or
            ($whitelistApplication and!$whitelistApplication->isAccepted) or
            !WebUserHelper::checkWebUserConnectionWithFiveMUser(Auth::user()->id) or
            !Auth::user()->fiveMUser()->first()
        ) {
            return false;
        }

        return true;
    }
}
