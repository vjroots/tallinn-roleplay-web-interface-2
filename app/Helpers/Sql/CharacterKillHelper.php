<?php
namespace App\Helpers\Sql;

use App\OwnedShops;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Include methods that help delete FiveM user data from database
 */
class CharacterKillHelper
{

    /**
     * @var string
     */
    const identifier = 'identifier';

    /**
     * @var string
     */
    const owner = 'owner';

    /**
     * @param $steamId
     * @return int
     */
    public static function removeAddOnAccountData($steamId)
    {

        return self::removeFromDatabase('addon_account_data', $steamId, self::owner);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeAddOnInventoryItems($steamId)
    {

        return self::removeFromDatabase('addon_inventory_items', $steamId, self::owner);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeBills($steamId)
    {

        return self::removeFromDatabase('billing', $steamId, self::identifier);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeCreatedBills($steamId)
    {

        return self::removeFromDatabase('billing', $steamId, 'sender');
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeCharacters($steamId)
    {

        return self::removeFromDatabase('characters', $steamId, self::identifier);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeCriminalRecords($steamId)
    {

        return self::removeFromDatabase('criminal_records', $steamId, 'user_identifier');
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeDataStoreData($steamId)
    {

        return self::removeFromDatabase('datastore_data', $steamId, self::owner);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeMedicalHistory($steamId)
    {

        return self::removeFromDatabase('medical_histories', $steamId, 'user_identifier');
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeOwnedDock($steamId)
    {
        return self::removeFromDatabase('owned_dock', $steamId, self::owner);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeOwnedProperties($steamId)
    {
        return self::removeFromDatabase('owned_properties', $steamId, self::owner);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeOwnedVehicles($steamId)
    {
        return self::removeFromDatabase('owned_vehicles', $steamId, self::owner);
    }

    /**
     * @param $phoneNumber
     * @return int
     */
    public static function removePhoneCalls($phoneNumber)
    {
        $dialedCalls = self::removeFromDatabase('phone_calls', $phoneNumber, self::owner);
        $answeredCalls = self::removeFromDatabase('phone_calls', $phoneNumber, 'num');

        return $dialedCalls + $answeredCalls;
    }

    /**
     * @param $phoneNumber
     * @return int
     */
    public static function removePhoneMessages($phoneNumber)
    {
        $sentMessages = self::removeFromDatabase('phone_messages', $phoneNumber, 'transmitter');
        $answeredMessages = self::removeFromDatabase('phone_messages', $phoneNumber, 'receiver');

        return $sentMessages + $answeredMessages;
    }

    /**
     * @param $steamId
     * @param $phoneNumber
     * @return int
     */
    public static function removePhoneUserContacts($steamId, $phoneNumber)
    {
        $userContacts = self::removeFromDatabase('phone_users_contacts', $steamId, self::identifier);
        $inOtherUsersContact = self::removeFromDatabase('owned_vehicles', $phoneNumber, 'number');

        return $userContacts + $inOtherUsersContact;
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removePlayersTattoos($steamId)
    {

        return self::removeFromDatabase('playerstattoos', $steamId, self::identifier);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeUsers($steamId)
    {

        return self::removeFromDatabase('users', $steamId, self::identifier);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeUserAccounts($steamId)
    {

        return self::removeFromDatabase('user_accounts', $steamId, self::identifier);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeUserInventory($steamId)
    {

        return self::removeFromDatabase('user_inventory', $steamId, self::identifier);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeUserLicenses($steamId)
    {

        return self::removeFromDatabase('user_licenses', $steamId, self::owner);
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeWantedCharacters($steamId)
    {

        return self::removeFromDatabase('wanted_characters', $steamId, 'user_identifier');
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeCreatedCriminalRecords($steamId)
    {

        return self::removeFromDatabase('criminal_records', $steamId, 'police_officer_id');
    }

    /**
     * @param $steamId
     * @return int
     */
    public static function removeCreatedMedicalRecords($steamId)
    {

        return self::removeFromDatabase('medical_histories', $steamId, 'ems_worker_id');
    }

    /**
     * @param $steamId
     */
    public static function removeCarSellHistory($steamId)
    {

        self::removeFromDatabase('car_sell_requests', $steamId, 'sellerId');
        self::removeFromDatabase('car_sell_requests', $steamId, 'buyerId');
        self::removeFromDatabase('car_sell_logs', $steamId, 'seller');
        self::removeFromDatabase('car_sell_logs', $steamId, 'buyer');

        return;
    }

    /**
     * @param $steamId
     */
    public static function removeSkills($steamId)
    {

        self::removeFromDatabase('softskills', $steamId, 'identifier');
    }

    /**
     * @param $steamId
     */
    public static function removeShop($steamId)
    {

        $ownedShops = OwnedShops::where('identifier', $steamId)->get();

        if($ownedShops) {
            foreach ($ownedShops as $shop) {
                self::removeFromDatabase('shops', $shop->ShopNumber, 'ShopNumber');
                $shop->identifier = null;
                $shop->money = null;
                $shop->money2 = null;
                $shop->ShopName = null;
                $shop->save();
            }
        }
    }

    /**
     * @param $table
     * @param $steamId
     * @param $identifierColumnName
     * @return int
     */
    private static function removeFromDatabase($table, $steamId, $identifierColumnName)
    {

        try {
            $results = DB::table($table)->where($identifierColumnName, '=', $steamId)->delete();
            Log::info($steamId . ' had ' . $results . 'row deleted from ' . $table);
        } catch (\Exception $exception) {
            Log::info($steamId . ' have nothing in ' . $table);
            $results = 0;
        }

        return $results;
    }
}
