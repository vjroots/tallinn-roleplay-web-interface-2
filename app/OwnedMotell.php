<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnedMotell extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'owned_motell';
}
