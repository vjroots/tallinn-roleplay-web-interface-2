<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WantedCharacters
 */
class WantedCharacters extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {

        return $this->belongsTo(User::class);
    }
}
