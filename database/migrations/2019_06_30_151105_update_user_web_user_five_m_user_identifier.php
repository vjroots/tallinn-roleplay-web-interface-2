<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserWebUserFiveMUserIdentifier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('user_web_user', function (Blueprint $table) {
            $table->renameColumn('user_id', 'user_identifier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('user_web_user', function (Blueprint $table) {
            $table->renameColumn('user_identifier', 'user_id');
        });
    }
}
