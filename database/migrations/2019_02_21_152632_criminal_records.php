<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriminalRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('criminal_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('criminal_steam_id');
            $table->string('character_name');
            $table->string('offence_type');
            $table->string('fine')->nullable();
            $table->string('jail_time')->nullable();
            $table->boolean('warning_car');
            $table->boolean('warning_driver_license');
            $table->boolean('warning_gun_license');
            $table->string('summary');
            $table->string('officer_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criminal_records');
    }
}
